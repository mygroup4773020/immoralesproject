/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.immoralesproject;

/**
 *
 * @author IvanMorales_IOC
 * «He aprés molt fent el mòdul 8 de DAW encara que hagi estat dur!!!
 * 
 */

    
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class Prova {

    // Definir las variables de configuración
    private static final String API_KEY = "YOU_API_KEY_HERE";
    private static final String MODEL_NAME = "gpt-3.5-turbo";
    private static final String CONTEXT = "{\"role\": \"system\", \"content\": \"You are a helpful assistant.\"}";

    private static final String GREEN = "\033[32m";
    private static final String RESET = "\033[0m";
    private static String PREGUNTA_PROMPT = "Buenas, soy ChatGPT, necesitas algo? Pregunta:";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            promptUser();

            String content = scanner.nextLine();

            // Realizar la solicitud a la API de ChatGPT
            try {
                String response = sendRequest(content);

                // Extraer la parte del campo "content"
                String contenido = extractContent(response);

                // Imprimir el contenido formateado
                System.out.println(GREEN + "ChatGPT: " + RESET + contenido);
            } catch (IOException e) {
                System.err.println("Error al enviar la solicitud: " + e.getMessage());
            }
        }
    }

    private static void promptUser() {
        System.out.print(GREEN + PREGUNTA_PROMPT + RESET + "\n ");
        PREGUNTA_PROMPT = "\nTienes mas dudas? Adelante, pregunta:";
    }

    private static String sendRequest(String content) throws IOException {
        URL url = new URL("https://api.openai.com/v1/chat/completions");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Authorization", "Bearer " + API_KEY);
        con.setDoOutput(true);

        String jsonInputString = "{\"model\": \"" + MODEL_NAME + "\", \"messages\": [ " + CONTEXT + ", {\"role\": \"user\", \"content\": \"" + content + "\"}]}";

        con.getOutputStream().write(jsonInputString.getBytes());

        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                response.append(line);
            }
            return response.toString();
        }
    }

    private static String extractContent(String response) {
        String contenido = response.split("\"content\":")[1];
        contenido = contenido.replaceAll("[{}]", "").trim();
        contenido = contenido.replaceAll("^\"|\"$", "").trim();
        contenido = contenido.replaceAll("\\\\\"", "\"").trim();
        contenido = contenido.replaceAll("\\\\n", "\n").trim();
        return contenido;
    }
}

    
